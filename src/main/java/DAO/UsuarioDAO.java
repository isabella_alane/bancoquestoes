package DAO;

import DAO.Conexao;
import modelo.TbUsuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.TbPerfil;
/**
 *
 * @author aluno
 */
public class UsuarioDAO {

    public TbUsuario inserirTbUsuario(TbUsuario u) {
        TbUsuario retorno = new TbUsuario();
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into tb_usuario(nm_usuario, nm_login, ds_senha, id_perfil) values (?,?,?,?) returning id_usuario;") ;
            ps.setString(1, u.getNmUsuario());
            ps.setString(2, u.getNmLogin());
            ps.setString(3, u.getDsSenha());
            ps.setInt(4, u.getIdPerfil().getIdPerfil());
            ResultSet rs = ps.executeQuery();
             if(rs.next()){
                retorno.setIdUsuario(rs.getInt("id_usuario"));
            }
            c.close();
            return retorno;
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            return null;
        }
    }
     public void modificarTbUsuario(TbUsuario u) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("update tb_usuario set  nm_usuario = ?, nm_login = ?, ds_senha = ?, id_perfil = ? where id_usuario = ?");
            ps.setString(1, u.getNmUsuario());
            ps.setString(2, u.getNmLogin());
            ps.setString(3, u.getDsSenha());
            ps.setInt(4, u.getIdPerfil().getIdPerfil());
            ps.setInt(5, u.getIdUsuario());
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //O objetivo é retornar um ArrayList com todos os nomes de usuários cadastrados no BD
    public ArrayList<String> consultarNomeTbUsuario(){
        try {
            ArrayList<String> nomes = new ArrayList<>();
            Connection c = Conexao.obterConexao();
            String sql = "select nm_login from tb_usuario";
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery(sql);
            while(rs.next()){
                nomes.add(rs.getString("nm_login"));
            }
            c.close();
            return nomes;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public String consultarSenhaTbUsuario(String nm_login){
        try {
            String senha;
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("select ds_senha from tb_usuario where nm_login = ?");
            ps.setString(1, nm_login);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                senha = rs.getString("ds_senha");
            } else {
                senha = "";
            }
            c.close();
            return senha;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public TbUsuario loginTbUsuario(TbUsuario fazerLogin){
        TbUsuario retorno = null;
        Connection c = Conexao.obterConexao();
        try {
            String sql = "select * from tb_usuario where nm_login = ? and ds_senha = ?";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, fazerLogin.getNmLogin());
            ps.setString(2, fazerLogin.getDsSenha());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                System.out.println(rs);
                c.close();
                retorno = new TbUsuario();
                retorno.setIdUsuario(rs.getInt("id_usuario"));
                retorno.setIdPerfil(new TbPerfil(rs.getInt("id_perfil")));
                retorno.setNmUsuario(rs.getString("nm_usuario"));
                retorno.setNmLogin(rs.getString("nm_login"));
                return retorno;
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            try {
                c.close();
            } catch (SQLException ex1) {
                Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex1);
            }
            return retorno;
        }
    }
    
}