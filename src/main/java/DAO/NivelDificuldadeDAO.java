/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.TbDisciplina;
import modelo.TbNivelDificuldade;

/**
 *
 * @author aluno
 */
public class NivelDificuldadeDAO {
    /*public List<TbNivelDificuldade> listaNiveis(){
    
        try {
            List<TbNivelDificuldade> retorno = new ArrayList<TbNivelDificuldade>(){};
            
            Connection c = Conexao.obterConexao();
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery("select id_nivel, ds_nivel, nm_nivel from tb_nivel_dificuldade");
            while (rs.next()){
                TbNivelDificuldade atual = new TbNivelDificuldade();
                atual.setIdNivel(rs.getInt("id_nivel"));
                atual.setDsNivel(rs.getString("ds_nivel"));
                atual.setNmNivel(rs.getString("nm_nivel"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(NivelDificuldadeDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }*/
    public List<TbNivelDificuldade> obterNiveis(){
        List<TbNivelDificuldade> retorno = new ArrayList<TbNivelDificuldade>(){};
        Connection c = null;
        try {
            c = Conexao.obterConexao();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery("select * from tb_nivel_dificuldade");
            while(rs.next()){
                TbNivelDificuldade atual = new TbNivelDificuldade();
                atual.setIdNivel(rs.getInt("id_nivel"));
                atual.setNmNivel(rs.getString("nm_nivel"));
                atual.setDsNivel(rs.getString("ds_nivel"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            try {
                Logger.getLogger(NivelDificuldadeDAO.class.getName()).log(Level.SEVERE, null, ex);
                c.close();
                return null;
            } catch (SQLException ex1) {
                Logger.getLogger(NivelDificuldadeDAO.class.getName()).log(Level.SEVERE, null, ex1);
                return null;
            }
        }
    }
}
