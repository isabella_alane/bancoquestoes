/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.TbQuestao;
import modelo.TbResposta;

/**
 *
 * @author Isabella
 */
public class RespostaDAO {
    public TbResposta inserirTbResposta(TbResposta r) {
        TbResposta retorno = new TbResposta();
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into tb_resposta(id_questao, fl_correta, cd_alternativa, nm_resposta) values (?,?,?,?) returning id_resposta;") ;
            ps.setInt(1 , r.getIdQuestao());
            ps.setBoolean(2, r.getFlCorreta());
            ps.setString(3, r.getCdAlternativa());
            ps.setString(4, r.getNmResposta());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                retorno.setIdResposta(rs.getInt("id_resposta"));
            }
            c.close();
            return retorno;

            
        }  catch (SQLException ex) {
            Logger.getLogger(RespostaDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            return null;
        }
    }
     public void modificarTbResposta(TbResposta r) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("update tb_resposta set  fl_correta = ?, cd_alternativa = ? , nm_resposta = ? where id_questao = ?");
            ps.setBoolean(1, r.getFlCorreta());
            ps.setString(2, r.getCdAlternativa());
            ps.setString(3, r.getNmResposta());
            ps.setInt(4, r.getIdQuestao());
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(RespostaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      public ArrayList<TbResposta> chamarListarRespostasFiltradas(int id_questao) {
        try {
            PreparedStatement st
                    = Conexao.obterConexao().prepareStatement(
                            "select * from tb_resposta WHERE id_questao=?");
            st.setInt(1, id_questao);
            ResultSet rs = st.executeQuery();
            ArrayList<TbResposta> lista = new ArrayList<TbResposta>();
            TbResposta resposta = null;
            while (rs.next()) {
                resposta = new TbResposta();
                resposta.setIdResposta(rs.getInt("id_resposta"));
                resposta.setNmResposta(rs.getString("nm_resposta"));
                resposta.setIdQuestao(rs.getInt("id_questao"));
                lista.add(resposta);
            }
            rs.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(RespostaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
