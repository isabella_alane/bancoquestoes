
package DAO;
import modelo.TbDisciplina;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import modelo.TbAreaEnem;

public class DisciplinaDAO {
    
    public TbDisciplina inserirTbDisciplina(TbDisciplina d) {
        TbDisciplina retorno = new TbDisciplina();
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into tb_disciplina(id_area_conhecimento, id_area_enem, nm_disciplina) values (?,?,?) returning id_disciplina;") ;
            ps.setInt(1, d.getIdAreaConhecimento());
            ps.setInt(2, d.getIdAreaEnem());
            ps.setString(3, d.getNmDisciplina());
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                retorno.setIdDisciplina(rs.getInt("id_disciplina"));
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(DisciplinaDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            return null;
        }
    }
    public void modificarTbDisciplina(TbDisciplina d) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("update tb_disciplina set  id_area_conhecimento = ?, id_area_enem = ?, nm_disciplina = ? where id_disciplina =?");
            ps.setInt(1, d.getIdDisciplina());
            ps.setInt(2, d.getIdAreaConhecimento());
            ps.setInt(3, d.getIdAreaEnem());
            ps.setString(4, d.getNmDisciplina());
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(DisciplinaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<TbDisciplina> listaTodasDisciplinas(){
    
        try {
            List<TbDisciplina> retorno = new ArrayList<TbDisciplina>();
            
            Connection c = Conexao.obterConexao();
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery("select id_area_enem, id_area_conhecimento, id_disciplina, nm_disciplina   from tb_disciplina order by nm_disciplina");
            while (rs.next()){
            
                TbDisciplina atual = new TbDisciplina();
                atual.setIdAreaEnem(rs.getInt("id_area_enem"));
                atual.setIdAreaConhecimento(rs.getInt("id_area_conhecimento"));
                atual.setIdDisciplina(rs.getInt("id_disciplina"));
                atual.setNmDisciplina(rs.getString("nm_disciplina"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(DisciplinaDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public ArrayList<TbDisciplina> retrieve(String parteDescr) throws Exception{
        PreparedStatement st = 
                Conexao.obterConexao().prepareStatement(
                        "select * from tb_disciplina WHERE nm_questao like ?");
        st.setString(1, "%"+parteDescr+"%");
        ResultSet rs = st.executeQuery();
        ArrayList<TbDisciplina> lista = new ArrayList<TbDisciplina>();
        TbDisciplina disciplina = null;
        while(rs.next()){
            disciplina.setIdAreaConhecimento(rs.getInt(0));
            disciplina.setIdAreaEnem(rs.getInt(1));
            disciplina.setNmDisciplina(rs.getString(3));
        }
       rs.close(); 
       if (disciplina == null){
           throw new Exception("Registro com  "+parteDescr+" na descricao inexistente");
       }
       return lista;
    }
     public ArrayList<TbDisciplina> chamarListaDisciplinasFiltradas(int id_area_conhecimento){
    
        try {

            
            Connection c = Conexao.obterConexao();
            //Statement ps = c.createStatement();
            PreparedStatement ps = c.prepareStatement("select  id_disciplina, nm_disciplina where id_area_conhecimento=?");
            ps.setInt(1,id_area_conhecimento);
            ArrayList<TbDisciplina> retorno = new ArrayList<TbDisciplina>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
            
                TbDisciplina atual = new TbDisciplina();
                atual.setIdAreaEnem(rs.getInt("id_area_enem"));
                atual.setIdAreaConhecimento(rs.getInt("id_area_conhecimento"));
                atual.setIdDisciplina(rs.getInt("id_disciplina"));
                atual.setNmDisciplina(rs.getString("nm_disciplina"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(DisciplinaDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
     public ArrayList<TbDisciplina> chamarListaDisciplinaPorAreaEnem(int id_area_enem) {
        try {
            PreparedStatement st
                    = Conexao.obterConexao().prepareStatement(
                            "select * from tb_disciplina WHERE id_area_enem=?");
            st.setInt(1,id_area_enem);
            ResultSet rs = st.executeQuery();
            ArrayList<TbDisciplina> lista = new ArrayList<TbDisciplina>();
            TbDisciplina disciplina = null;
            while (rs.next()) {
                disciplina = new TbDisciplina();
                disciplina.setIdAreaEnem(rs.getInt("id_area_enem"));
                disciplina.setIdAreaConhecimento(rs.getInt("id_area_conhecimento"));
                disciplina.setIdDisciplina(rs.getInt("id_disciplina"));
                disciplina.setNmDisciplina(rs.getString("nm_disciplina"));
                lista.add(disciplina);
            }
            rs.close();
            if (disciplina == null) {
                //throw new Exception("Registro com  " + id_disciplina + " na descricao inexistente");
                return null;
            }
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(DisciplinaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}




