/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.TbQuestao;

/**
 *
 * @author Isabella
 */
public class QuestaoDAO {

    public TbQuestao inserirTbQuestao(TbQuestao q) {
        TbQuestao retorno = new TbQuestao();
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into tb_questao(id_disciplina,ds_texto_questao, nm_questao, id_usuario,id_nivel) values (?,?,?,?,?) returning id_questao;");
            ps.setInt(1, q.getIdDisciplina());
            ps.setString(2, q.getDsTextoQuestao());
            ps.setString(3, q.getNmQuestao());
            ps.setInt(4, q.getIdUsuario());
            ps.setInt(5, q.getIdNivel());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                retorno.setIdQuestao(rs.getInt("id_questao"));
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(QuestaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public void modificarTbQuestao(TbQuestao q) {
        try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("update tb_questao set ds_texto_questao = ?, nm_questao = ?, id_nivel =?, id_usuario=? where id_questao = ?");
            ps.setString(1, q.getDsTextoQuestao());
            ps.setString(2, q.getNmQuestao());
            ps.setInt(3, q.getIdQuestao());
            ps.setInt(4, q.getIdNivel());
            ps.setInt(5, q.getIdUsuario());
            ps.executeUpdate();
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(QuestaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void ocultarTbQuestao (int id){
          try {
            Connection c = Conexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("update tb_questao set id_questao = ? where id_questao = ?");
            ps.setInt(1, (id * -1));
            ps.setInt(2, id);
            ps.executeUpdate();
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(QuestaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
     
     
    public List<TbQuestao> listaTodasQuestoes() {

        try {
            List<TbQuestao> retorno = new ArrayList<>();

            Connection c = Conexao.obterConexao();
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery("select id_questao, nm_questao,ds_texto_questao, id_disciplina, id_nivel, id_usuario from tb_questao where id_questao > 0 order by id_questao");
            while (rs.next()) {

                TbQuestao atual = new TbQuestao();
                atual.setIdQuestao(rs.getInt("id_questao"));
                atual.setNmQuestao(rs.getString("nm_questao"));
                atual.setDsTextoQuestao(rs.getString("ds_texto_questao"));
                //atual.setIdResposta(rs.getInt("id_resposta"));
                atual.setIdDisciplina(rs.getInt("id_disciplina"));
                atual.setIdNivel(rs.getInt("id_nivel"));
                atual.setIdUsuario(rs.getInt("id_usuario"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(QuestaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<TbQuestao> retrieve(String parteDescr) throws Exception {
        PreparedStatement st
                = Conexao.obterConexao().prepareStatement(
                        "select * from tb_questao WHERE nm_questao like ?");
        st.setString(1, "%" + parteDescr + "%");
        ResultSet rs = st.executeQuery();
        ArrayList<TbQuestao> lista = new ArrayList<TbQuestao>();
        TbQuestao questao = null;
        while (rs.next()) {
            questao = new TbQuestao();
            questao.setDsTextoQuestao(rs.getString(1));
            questao.setDsTextoQuestao(rs.getString(2));
            questao.setIdDisciplina(rs.getInt(3));
            questao.setIdNivel(rs.getInt(4));
            questao.setNmQuestao(rs.getString(5));
            questao.setIdUsuario(rs.getInt(6));
        }
       rs.close(); 
       if (questao == null){
           throw new Exception("Registro com  "+parteDescr+" na descricao inexistente");
       }
       return lista;
       }  
    
    public ArrayList<TbQuestao> chamarListaQuestaoFiltradas(int id_disciplina) {
        try {
            PreparedStatement st
                    = Conexao.obterConexao().prepareStatement(
                            "select * from tb_questao WHERE id_disciplina=?");
            System.out.println(st);
            st.setInt(1,id_disciplina);
            ResultSet rs = st.executeQuery();
            ArrayList<TbQuestao> lista = new ArrayList<TbQuestao>();
            TbQuestao questao = null;
            while (rs.next()) {
                questao = new TbQuestao();
                questao.setDsTextoQuestao(rs.getString("ds_texto_questao"));
                questao.setIdDisciplina(rs.getInt("id_disciplina"));
                questao.setIdNivel(rs.getInt("id_nivel"));
                questao.setNmQuestao(rs.getString("nm_questao"));
                questao.setIdUsuario(rs.getInt("id_usuario"));
                lista.add(questao);
            }
            rs.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(QuestaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
