/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.TbAreaConhecimento;
import modelo.TbAreaEnem;

/**
 *
 * @author admin
 */
public class AreaDAO {


    public List<TbAreaConhecimento> obterAreasConhecimento(){
        Connection c = null;
        try {
            List<TbAreaConhecimento> retorno = new ArrayList<TbAreaConhecimento>();
            c = Conexao.obterConexao();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery("select * from tb_area_conhecimento order by nm_area_conhecimento");
            while(rs.next()){
                TbAreaConhecimento atual = new TbAreaConhecimento();
                atual.setIdAreaConhecimento(rs.getInt("id_area_conhecimento"));
                atual.setNmAreaConhecimento(rs.getString("nm_area_conhecimento"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            try {
                Logger.getLogger(AreaDAO.class.getName()).log(Level.SEVERE, null, ex);
                c.close();
                return null;
            } catch (SQLException ex1) {
                Logger.getLogger(AreaDAO.class.getName()).log(Level.SEVERE, null, ex1);
                return null;
            }
        }
    }
    public List<TbAreaEnem> obterAreasENEM(){
        List<TbAreaEnem> retorno = new ArrayList<TbAreaEnem>();
        Connection c = null;
        try {
            c = Conexao.obterConexao();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery("select * from tb_area_enem order by nm_area_enem");
            while(rs.next()){
                TbAreaEnem atual = new TbAreaEnem();
                atual.setIdAreaEnem(rs.getInt("id_area_enem"));
                atual.setNmAreaEnem(rs.getString("nm_area_enem"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            try {
                Logger.getLogger(AreaDAO.class.getName()).log(Level.SEVERE, null, ex);
                c.close();
                return null;
            } catch (SQLException ex1) {
                Logger.getLogger(AreaDAO.class.getName()).log(Level.SEVERE, null, ex1);
                return null;
            }
        }
    }
    
}
