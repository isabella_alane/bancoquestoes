package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    public static Connection obterConexao() {
        try {
            Class.forName("org.postgresql.Driver");
            String usuario = "bancoquestoes";
            String senha = "bancoquestoes";
            String banco = "jdbc:postgresql://200.18.128.54/bancoquestoes";
            return DriverManager.getConnection(banco, usuario, senha);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}