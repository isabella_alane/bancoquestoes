/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.TbPerfil;

/**
 *
 * @author aluno
 */
public class PerfilDAO {
    public List<TbPerfil> listaPerfis(){
    
        try {
            List<TbPerfil> retorno = new ArrayList<TbPerfil>();
            
            Connection c = Conexao.obterConexao();
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery("select id_perfil, nm_perfil, ds_acesso from tb_perfil order by nm_perfil");
            while (rs.next()){
                TbPerfil atual = new TbPerfil();
                atual.setIdPerfil(rs.getInt("id_perfil"));
                atual.setNmPerfil(rs.getString("nm_perfil"));
                atual.setDsAcesso(rs.getString("ds_acesso"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            Logger.getLogger(NivelDificuldadeDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    public List<TbPerfil> obterPerfis(){
        List<TbPerfil> retorno = new ArrayList<TbPerfil>();
        Connection c = null;
        try {
             c = Conexao.obterConexao();
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery("select * from tb_perfil");
            while(rs.next()){
                TbPerfil atual = new TbPerfil();
                atual.setIdPerfil(rs.getInt("id_perfil"));
                atual.setNmPerfil(rs.getString("nm_perfil"));
                retorno.add(atual);
            }
            c.close();
            return retorno;
        } catch (SQLException ex) {
            try {
                Logger.getLogger(AreaDAO.class.getName()).log(Level.SEVERE, null, ex);
                c.close();
                return null;
            } catch (SQLException ex1) {
                Logger.getLogger(AreaDAO.class.getName()).log(Level.SEVERE, null, ex1);
                return null;
            }
        }
    }
}
