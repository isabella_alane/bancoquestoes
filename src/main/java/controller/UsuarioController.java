/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import DAO.UsuarioDAO;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import modelo.TbPerfil;
import modelo.TbUsuario;

/**
 *
 * @author Isabella
 */
@ManagedBean(name = "usuarioController")
@SessionScoped
public class UsuarioController {
    private String mensagem = "";
    private boolean usuarioInserido = false;
    private TbUsuario usuario = new TbUsuario();

    public void inserirTbUsuario() {
        UsuarioDAO u = new UsuarioDAO();
        usuario = u.inserirTbUsuario(usuario);
        if (usuario != null && usuario.getIdUsuario()!= 0.0) {
            this.setUsuarioInserido(true);
            this.setMensagem(this.usuario.getNmUsuario()+ " inserido com sucesso!");            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Usuário inserido com sucesso!."));
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Erro no cadastro."));
        }
        
        
        
    }
     public void atualizarTbUsuario() {
        UsuarioDAO u = new UsuarioDAO();
        u.modificarTbUsuario(usuario);
        if (usuario != null && usuario.getIdUsuario()!= 0.0) {
            this.setUsuarioInserido(true);
            this.setMensagem(this.usuario.getNmUsuario()+ " atualizado com sucesso!");
        }
    }

    public TbUsuario getUsuario() {
        return usuario;
    }

    public void setUsuario(TbUsuario usuario) {
        this.usuario = usuario;
    }

    public boolean isUusaroiInserido() {
        return usuarioInserido;
    }

    public void setUsuarioInserido(boolean usuarioInserido) {
        this.usuarioInserido = usuarioInserido;
    }
    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public UsuarioController() {
        
        usuario.setIdPerfil(new TbPerfil());
    }
    public static UsuarioController getInstance() {
        UsuarioController atual = (UsuarioController) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("usuarioController");
        return atual;
    }
    
}
    

