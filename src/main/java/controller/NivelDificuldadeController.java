/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.AreaDAO;
import DAO.NivelDificuldadeDAO;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.TbAreaConhecimento;
import modelo.TbAreaEnem;
import modelo.TbDisciplina;
import modelo.TbNivelDificuldade;

@ManagedBean(name = "nivelController")
@SessionScoped
/**
 *
 * @author aluno
 */
public class NivelDificuldadeController implements Serializable {
    private String mensagem = "";
    private TbNivelDificuldade nivel = new TbNivelDificuldade();
    private List<TbNivelDificuldade> listaNivelDificuldade;

    public NivelDificuldadeController() {
        nivel = new TbNivelDificuldade();
        this.atualizarListaNiveis();
    }

    public String getMensagem() {
        return mensagem;
    }

    public TbNivelDificuldade getNivel() {
        return nivel;
    }

    public void setNivel(TbNivelDificuldade nivel) {
        this.nivel = nivel;
    }

    public List<TbNivelDificuldade> getListaNivelDificuldade() {
        return listaNivelDificuldade;
    }

    public void setListaNivelDificuldade(List<TbNivelDificuldade> listaNivelDificuldade) {
        this.listaNivelDificuldade = listaNivelDificuldade;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    public List<TbNivelDificuldade> getListaTbNivelDificuldade() {
        return listaNivelDificuldade;
    }
    public void atualizarListaNiveis() {
        NivelDificuldadeDAO nd = new NivelDificuldadeDAO();
        this.listaNivelDificuldade= nd.obterNiveis();
                
    }
    public void setListaNiveisDificuldade(List<TbNivelDificuldade> listaNivelDificuldade) {
        this.listaNivelDificuldade = listaNivelDificuldade;
    }
    public String getNivelDificuldadeByIndex(int index){
        //if(this.listaNivelDificuldade==null)return"";
        for(TbNivelDificuldade atual : this.listaNivelDificuldade){
            if(atual!=null && atual.getIdNivel()!=null){
                if(atual.getIdNivel().intValue()==index){
                    return atual.getNmNivel();
                }
            }
        }
        return "";
    }
        private void exibirMensagem(String mensagemASerExibida) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", mensagemASerExibida));
    }
    public static NivelDificuldadeController getInstance() {
        NivelDificuldadeController atual = (NivelDificuldadeController) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("nivelController");
        return atual;
    }
    
}
