/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.AreaDAO;
import DAO.DisciplinaDAO;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import modelo.TbAreaEnem;
import modelo.TbDisciplina;

/**
 *
 * @author aluno
 */
@ManagedBean(name = "areaEnemController")
@SessionScoped
public class AreaEnemController {
     private String mensagem = "";
     private boolean areaEnemInserida = false;
     private List<TbAreaEnem> listaAreaEnem;


    public List<TbAreaEnem> chamarListaAreaEnem(){
        AreaDAO ad = new AreaDAO();
        this.listaAreaEnem = ad.obterAreasENEM();
        return this.listaAreaEnem;
    }

}
