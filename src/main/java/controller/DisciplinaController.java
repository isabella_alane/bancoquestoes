/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.AreaDAO;
import DAO.DisciplinaDAO;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.TbAreaConhecimento;
import modelo.TbAreaEnem;
import modelo.TbDisciplina;

/**
 *
 * @author aluno
 */
@ManagedBean(name = "disciplinaController")
@SessionScoped
public class DisciplinaController implements  Serializable {

    private String mensagem = "";
    private boolean disciplinaInserida = false;
    private TbDisciplina disciplina = new TbDisciplina();
    private List<TbDisciplina> listaDisciplinasAtuais;
    private List<TbDisciplina> listaDisciplinasFiltradas;
    private List<TbAreaConhecimento> listaAreaConhecimento;
    private List<TbAreaEnem> listaAreaEnem;
     private List<TbDisciplina> listaDisciplinasPorAreaEnem;
    public void inserirTbDisciplina() {
        DisciplinaDAO d = new DisciplinaDAO();
        System.out.println(disciplina);
        TbDisciplina inserida = d.inserirTbDisciplina(disciplina);
        if (inserida != null && inserida.getIdDisciplina() != 0.0) {
            this.setDisciplinaInserida(true);
            this.setMensagem(this.disciplina.getNmDisciplina() + " inserida com sucesso! Código gerado: " + inserida.getIdDisciplina());
            this.exibirMensagem(this.disciplina.getNmDisciplina() + " inserida com sucesso! Código gerado: " + inserida.getIdDisciplina());
        }
        this.atualizaListaDisciplinas();
    }

    public void atualizarTbDisciplina() {
        DisciplinaDAO d = new DisciplinaDAO();
        d.modificarTbDisciplina(disciplina);
        if (disciplina != null && disciplina.getIdDisciplina() != 0.0) {
            this.setDisciplinaInserida(true);
            this.setMensagem(this.disciplina.getNmDisciplina() + " modificada com sucesso!");
        }
    }

    public TbDisciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(TbDisciplina disciplina) {
        this.disciplina = disciplina;
    }

    public boolean isDiscilinaInserida() {
        return disciplinaInserida;
    }

    public void setDisciplinaInserida(boolean disciplinaInserida) {
        this.disciplinaInserida = disciplinaInserida;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public List<TbDisciplina> getListaDisciplinasAtuais() {
        return listaDisciplinasAtuais;
    }

    public void setListaDisciplinasAtuais(List<TbDisciplina> listaDisciplinasAtuais) {
        this.listaDisciplinasAtuais = listaDisciplinasAtuais;
    }

    public void atualizaListaDisciplinas() {
        DisciplinaDAO dd = new DisciplinaDAO();
        this.listaDisciplinasAtuais = dd.listaTodasDisciplinas();
    }
    public void atualizaListaAreas() {
        AreaDAO ad = new AreaDAO();
        this.listaAreaEnem=ad.obterAreasENEM();
        this.listaAreaConhecimento=ad.obterAreasConhecimento();
                
    }

    public List<TbDisciplina> getListaDisciplinasFiltradas() {
        return listaDisciplinasFiltradas;
    }

    public void setListaDisciplinasFiltradas(List<TbDisciplina> listaDisciplinasFiltradas) {
        this.listaDisciplinasFiltradas = listaDisciplinasFiltradas;
    }

    private void exibirMensagem(String mensagemASerExibida) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", mensagemASerExibida));
    }

    public List<TbAreaConhecimento> getListaAreaConhecimento() {
        return listaAreaConhecimento;
    }

    public void setListaAreaConhecimento(List<TbAreaConhecimento> listaAreaConhecimento) {
        this.listaAreaConhecimento = listaAreaConhecimento;
    }

    public List<TbAreaEnem> getListaAreaEnem() {
        return listaAreaEnem;
    }

    public void setListaAreaEnem(List<TbAreaEnem> listaAreaEnem) {
        this.listaAreaEnem = listaAreaEnem;
    }

    public DisciplinaController() {
        disciplina = new TbDisciplina();
        this.atualizaListaDisciplinas();
        this.atualizaListaAreas();
    }

    public String getAreaEnemByIndex(int index){
        if(this.listaAreaEnem==null)return"";
        for(TbAreaEnem atual : this.listaAreaEnem){
            if(atual!=null && atual.getIdAreaEnem()!=null){
            if(atual.getIdAreaEnem().intValue()==index){
                return atual.getNmAreaEnem();
            }
            }
        }
        return "";
    }
    
    public String getAreaConhecimentoByIndex(int index){
        if(this.listaAreaConhecimento==null)return"";
        for(TbAreaConhecimento atual : this.listaAreaConhecimento){
            if(atual!=null && atual.getIdAreaConhecimento()!=null){
            if(atual.getIdAreaConhecimento().intValue()==index){
                return atual.getNmAreaConhecimento();
            }
            }
        }
        return "";
    }
    public List<TbDisciplina> chamarListarDisciplinaFiltradas(int id_area_conhecimento){
        DisciplinaDAO dd = new DisciplinaDAO();
        this.listaDisciplinasFiltradas = dd.chamarListaDisciplinasFiltradas(id_area_conhecimento);
        return this.listaDisciplinasFiltradas;
    }
   public static DisciplinaController getInstance() {
        DisciplinaController atual = (DisciplinaController) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("disciplinaController");
        return atual;
    }

    public String chamarListaDisciplinaPorAreaEnem(int id_area_enem){
        DisciplinaDAO ad = new DisciplinaDAO();
        this.listaDisciplinasPorAreaEnem = ad.chamarListaDisciplinaPorAreaEnem(id_area_enem);
        return "/section/disciplina/listarDisciplinaQuestoes.xhtml";
    }

    public List<TbDisciplina> getListaDisciplinasPorAreaEnem() {
        return listaDisciplinasPorAreaEnem;
    }

    public void setListaDisciplinasPorAreaEnem(List<TbDisciplina> listaDisciplinasPorAreaEnem) {
        this.listaDisciplinasPorAreaEnem = listaDisciplinasPorAreaEnem;
    }

}
