/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.NivelDificuldadeDAO;
import DAO.PerfilDAO;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.TbPerfil;
@ManagedBean(name = "perfilController")
@SessionScoped
/**
 *
 * @author aluno
 */
public class PerfilController implements Serializable {
    private String mensagem = "";
    private TbPerfil perfil = new TbPerfil();
    private List<TbPerfil> listaPerfis;
    
    public String getMensagem() {
        return mensagem;
    }

    public List<TbPerfil> getListaPerfis() {
        return listaPerfis;
    }

    public void setPerfil(TbPerfil perfil) {
        this.perfil = perfil;
    }
    public TbPerfil getPerfil() {
        return perfil;
    }
    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    public List<TbPerfil> getListaTbPerfil() {
        return listaPerfis;
    }

    public void setListaPerfis(List<TbPerfil> listaPerfis) {
        this.listaPerfis = listaPerfis;
    }

    public PerfilController() {
        perfil = new TbPerfil();
        this.atualizarListaPerfis();
    }
    public void atualizarListaPerfis() {
        PerfilDAO pd = new PerfilDAO();
        this.listaPerfis=pd.obterPerfis();
                
    }
    public String getPerfisByIndex(int index){
        if(this.listaPerfis==null)return"";
        for(TbPerfil atual : this.listaPerfis){
            if(atual!=null && atual.getIdPerfil()!=null){
            if(atual.getIdPerfil().intValue()==index){
                return atual.getNmPerfil();
            }
            }
        }
        return "";
    }
        private void exibirMensagem(String mensagemASerExibida) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", mensagemASerExibida));
    }
    public static PerfilController getInstance() {
        PerfilController atual = (PerfilController) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("perfilController");
        return atual;
    }    
}
