/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.QuestaoDAO;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.TbQuestao;

@ManagedBean(name = "questaoController")
@SessionScoped
/**
 *
 * @author Isabella
 */
public class QuestaoController {

    private String mensagem = "";
    private boolean questaoInserida = false;
    private TbQuestao questao = new TbQuestao();
    private List<TbQuestao> listaQuestoesAtuais;
    private boolean botaoApertado = false;
    private List<TbQuestao> listaQuestoesFiltradas;

    public List<TbQuestao> getListaQuestoesFiltradas() {
        return listaQuestoesFiltradas;
    }

    public void setListaQuestoesFiltradas(List<TbQuestao> listaQuestoesFiltradas) {
        this.listaQuestoesFiltradas = listaQuestoesFiltradas;
    }

    public void inserirTbQuestao() {
        if(botaoApertado){
            //pegar o usuário logado
            LoginController lc = LoginController.getInstance();
            int codigoUsuarioLogado=0;
            if(lc!=null){
                if(lc.getUsuarioLogin()!=null && lc.getUsuarioLogin().getIdUsuario()!=null){
                    codigoUsuarioLogado = lc.getUsuarioLogin().getIdUsuario();
                }
            }
            questao.setIdUsuario(codigoUsuarioLogado);
            QuestaoDAO q = new QuestaoDAO();
            TbQuestao inserido = q.inserirTbQuestao(questao);
            questao.setIdQuestao(inserido.getIdQuestao());
            if (questao != null && questao.getIdQuestao() != 0.0) {
                this.setQuestaoInserida(true);
                this.setMensagem(this.questao.getNmQuestao() + " inserida com sucesso!");
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Questão inserida com sucesso. Id da questão inserida: "+questao.getIdQuestao()));
            }else{
                this.setBotaoApertado(false);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Fatal", "erro ao inserir a questão. Verifique! "));
            }
        }
    }

    public void atualizarTbQuestao(TbQuestao questao) {
        QuestaoDAO q = new QuestaoDAO();
        q.modificarTbQuestao(questao);
        if (questao != null && questao.getIdQuestao() != 0.0) {
            this.setQuestaoInserida(true);
            this.setMensagem(this.questao.getIdQuestao() + " modificada com sucesso!");
        }
    }
    
    public void ocultarQuestao(int id){
        QuestaoDAO q = new QuestaoDAO();
        q.ocultarTbQuestao(id);
        System.out.println("OIIIIIIIIII DO LADO DO SERVIDOR: "+id);
    }
    
    public String chamarPaginaCadastroQuestoes() {
        LoginController l = LoginController.getInstance();
        if (l.isUsuarioLogado()) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de cadastro");
            this.setQuestaoInserida(false);
            return l.chamarPaginaCadastroQuestoes();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return l.chamarPaginaLogin();
        }
    }

    public String chamarPaginaCadastroDeResposta(){
        this.setBotaoApertado(true);
        this.inserirTbQuestao();
        LoginController l = LoginController.getInstance();
        if(this.questaoInserida)
            return l.chamarPaginaCadastroResposta();
        else{
            this.setBotaoApertado(false);
            return l.chamarPaginaCadastroQuestoes();
        }
    }

    public boolean getBotaoApertado() {
        return botaoApertado;
    }

    public void setBotaoApertado(boolean botaoApertado) {
        this.botaoApertado = botaoApertado;
    }
    public TbQuestao getQuestao() {
        return questao;
    }

    public void setQuestao(TbQuestao questao) {
        this.questao = questao;
    }

    public void setQuestaoInserida(boolean questaoInserida) {
        this.questaoInserida = questaoInserida;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public List<TbQuestao> getListaQuestoesAtuais() {
        return listaQuestoesAtuais;
    }

    public void setListaQuestoesAtuais(List<TbQuestao> listaQuestoesAtuais) {
        this.listaQuestoesAtuais = listaQuestoesAtuais;
    }

    private void atualizaListQuestoes() {
        QuestaoDAO qd = new QuestaoDAO();
        this.listaQuestoesAtuais = qd.listaTodasQuestoes();
    }

    public QuestaoController() {
        questao = new TbQuestao();
        this.atualizaListQuestoes();
    }

    public String getQuestaoByIndex(int index) {
        if (this.listaQuestoesAtuais == null) {
            return "";
        }
        for (TbQuestao atual : this.listaQuestoesAtuais) {
            if (atual != null && atual.getIdQuestao() != null) {
                if (atual.getIdQuestao().intValue() == index) {
                    return atual.getNmQuestao();
                }
            }
        }
        return "";
    }
    public List<TbQuestao> ListarQuestaoFiltradas(int id_disciplina){
        QuestaoDAO q = new QuestaoDAO();
        this.listaQuestoesFiltradas = q.chamarListaQuestaoFiltradas(id_disciplina);
        return this.listaQuestoesFiltradas;
    }
    public String chamarListarQuestaoFiltradas(int id_disciplina){
        QuestaoDAO q = new QuestaoDAO();
        System.out.println("DISCIPLINA SELECIONADA: "+id_disciplina);
        this.listaQuestoesFiltradas = q.chamarListaQuestaoFiltradas(id_disciplina);
        return new Paginas().getPaginaListaQuestaoFiltradas();
    }
    public static QuestaoController getInstance() {
        QuestaoController atual = (QuestaoController) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("questaoController");
        return atual;
    }
}
