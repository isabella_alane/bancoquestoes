/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.RespostaDAO;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.TbResposta;
@ManagedBean(name = "respostaController")
@SessionScoped
/**
 *
 * @author Isabella
 */
public class RespostaController {
    private String mensagem = "";
    private boolean respostaInserida = false;
    //Para cada caixa de texto no cadastro
    private TbResposta resposta = new TbResposta();
    private TbResposta resposta2 = new TbResposta();
    private TbResposta resposta3 = new TbResposta();
    private TbResposta resposta4 = new TbResposta();
    private List<TbResposta> listaRespostasFiltradas;
    
    public void inserirTbResposta(){
        int codigoDaQuestao = QuestaoController.getInstance().getQuestao().getIdQuestao();
        //Já tenho o código da questão acima... posso fazer o que quiser com o código
        resposta.setIdQuestao(codigoDaQuestao);
        RespostaDAO r = new RespostaDAO();
        r.inserirTbResposta(resposta);
        if (resposta!= null && resposta.getIdResposta()!= 0.0) {
            this.setRespostaInserida(true);
            this.setMensagem(this.resposta.getIdResposta()+ " inserida com sucesso!");            
        }
    }
    
    public void inserirTbResposta2(){
        int codigoDaQuestao = QuestaoController.getInstance().getQuestao().getIdQuestao();
        //Já tenho o código da questão acima... posso fazer o que quiser com o código
        resposta2.setIdQuestao(codigoDaQuestao);
        RespostaDAO r = new RespostaDAO();
        r.inserirTbResposta(resposta2);
        if (resposta2!= null && resposta2.getIdResposta()!= 0.0) {
            this.setRespostaInserida(true);
            this.setMensagem(this.resposta2.getIdResposta()+ " inserida com sucesso!");            
        }
    }
    
    public void inserirTbResposta3(){
        int codigoDaQuestao = QuestaoController.getInstance().getQuestao().getIdQuestao();
        //Já tenho o código da questão acima... posso fazer o que quiser com o código
        resposta3.setIdQuestao(codigoDaQuestao);
        RespostaDAO r = new RespostaDAO();
        r.inserirTbResposta(resposta3);
        if (resposta3!= null && resposta3.getIdResposta()!= 0.0) {
            this.setRespostaInserida(true);
            this.setMensagem(this.resposta3.getIdResposta()+ " inserida com sucesso!");            
        }
    }

    public List<TbResposta> getListaRespostasFiltradas() {
        return listaRespostasFiltradas;
    }

    public void setListaRespostasFiltradas(List<TbResposta> listaRespostasFiltradas) {
        this.listaRespostasFiltradas = listaRespostasFiltradas;
    }
    
    public void inserirTbResposta4(){
        int codigoDaQuestao = QuestaoController.getInstance().getQuestao().getIdQuestao();
        //Já tenho o código da questão acima... posso fazer o que quiser com o código
        resposta4.setIdQuestao(codigoDaQuestao);
        RespostaDAO r = new RespostaDAO();
        r.inserirTbResposta(resposta4);
        if (resposta4!= null && resposta4.getIdResposta()!= 0.0) {
            this.setRespostaInserida(true);
            this.setMensagem(this.resposta4.getIdResposta()+ " inserida com sucesso!");            
        }
    }

    public TbResposta getResposta2() {
        return resposta2;
    }

    public void setResposta2(TbResposta resposta2) {
        this.resposta2 = resposta2;
    }

    public TbResposta getResposta3() {
        return resposta3;
    }

    public void setResposta3(TbResposta resposta3) {
        this.resposta3 = resposta3;
    }

    public TbResposta getResposta4() {
        return resposta4;
    }

    public void setResposta4(TbResposta resposta4) {
        this.resposta4 = resposta4;
    }
     public void atualizarTbResposta() {
        RespostaDAO r = new RespostaDAO();
        r.modificarTbResposta(resposta);
        if (resposta!= null && resposta.getIdResposta()!= 0.0) {
            this.setRespostaInserida(true);
            this.setMensagem(this.resposta.getIdResposta()+ " odificada com sucesso!");            
        }
    }

    public TbResposta getResposta() {
        return resposta;
    }

    public void setResposta(TbResposta resposta) {
        this.resposta = resposta;
    }
     

    public void setRespostaInserida(boolean respostaInserida) {
        this.respostaInserida = respostaInserida;
    }
    public String getMensagem() {
        return mensagem;
    }

    public boolean isRespostaInserida() {
        return respostaInserida;
    }

    public RespostaController() {
        resposta = new TbResposta();
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    private void exibirMensagem(String mensagemASerExibida) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", mensagemASerExibida));
    }
    public static RespostaController getInstance() {
        RespostaController atual = (RespostaController) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("respostaController");
        return atual;
    }
    public List<TbResposta> ListarRespostasFiltradas(int id_questao){
        RespostaDAO r = new RespostaDAO();
        this.listaRespostasFiltradas = r.chamarListarRespostasFiltradas(id_questao);
        return this.listaRespostasFiltradas;
    }
    public String chamarListarRespostasFiltradas(int id_questao){
        RespostaDAO q = new RespostaDAO();
        this.listaRespostasFiltradas = q.chamarListarRespostasFiltradas(id_questao);
        return new Paginas().getPaginaListaQuestaoFiltradas();
    }
        public String getRespostaByIndex(int index){
        if(this.listaRespostasFiltradas==null)return"";
        for(TbResposta atual : this.listaRespostasFiltradas){
            if(atual!=null && atual.getIdResposta()!=null){
            if(atual.getIdResposta().intValue()==index){
                return atual.getNmResposta();
            }
            }
        }
        return "";
    }

}

