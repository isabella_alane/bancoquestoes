/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
@ManagedBean(name = "paginaController")
@SessionScoped
/**
 *
 * @author Hank
 */

public class Paginas {
 
    private String paginaCadastroQuestoes="/section/questao/cadastrarQuestao.xhtml";
    private String paginaCadastroDisciplinas="/section/disciplina/cadastrarDisciplina.xhtml";
    private String paginaPrincipal="/section/principal/principal.xhtml";
    //private String paginaCadastro="/exemploConteudo.xhtml";
    private String paginaCadastroResposta="/section/resposta/cadastrarResposta.xhtml";
    private String paginaCadastrarRespostasLista="/section/resposta/cadastrarRespostasLista.xhtml";
    private String paginaLogin="/section/login/login.xhtml";
    private String paginaCadastroUsuario="/section/usuario/cadastroUsuario.xhtml";
    private String paginaListaDisciplina="/section/disciplina/listarDisciplina.xhtml";
    private String paginaListaQuestao="/section/questao/listarQuestao.xhtml";
    private String paginaListaQuestaoENEM="/section/areaEnem/listarAreaEnem.xhtml";
    private String paginaListaQuestaoFiltradas="/section/questao/listarQuestaoFiltro.xhtml";
    private String paginaListaRespostaFiltradas= "/section/resposta/listarRespostaFiltro";

    public String getPaginaCadastrarRespostasLista() {
        return paginaCadastrarRespostasLista;
    }

    public String getPaginaListaRespostaFiltradas() {
        return paginaListaRespostaFiltradas;
    }

    public void setPaginaListaRespostaFiltradas(String paginaListaRespostaFiltradas) {
        this.paginaListaRespostaFiltradas = paginaListaRespostaFiltradas;
    }

    public void setPaginaCadastrarRespostasLista(String paginaCadastrarRespostasLista) {
        this.paginaCadastrarRespostasLista = paginaCadastrarRespostasLista;
    }

    public void setPaginaCadastroQuestoes(String paginaCadastro) {
        this.paginaCadastroQuestoes = paginaCadastro;
    }

    public Paginas() {
    }

    public String getPaginaCadastroResposta() {
        return paginaCadastroResposta;
    }

    public void setPaginaCadastroResposta(String paginaCadastroResposta) {
        this.paginaCadastroResposta = paginaCadastroResposta;
    }

    public String getPaginaListaDisciplina() {
        return paginaListaDisciplina;
    }

    public void setPaginaListaDisciplina(String paginaListaDisciplina) {
        this.paginaListaDisciplina = paginaListaDisciplina;
    }

    public String getPaginaListaQuestao() {
        return paginaListaQuestao;
    }

    public void setPaginaListaQuestao(String paginaListaQuestao) {
        this.paginaListaQuestao = paginaListaQuestao;
    }

    public String getPaginaCadastroQuestoes() {
        return paginaCadastroQuestoes;
    }

    public String getPaginaLogin() {
        return paginaLogin;
    }

    public void setPaginaLogin(String paginaLogin) {
        this.paginaLogin = paginaLogin;
    }

    public String chamarPaginaCadastroUsuario() {
        return getPaginaCadastroUsuario();
    }
    public String chamarPaginaListaDisciplinas() {
        return getPaginaListaDisciplina();
    }
    public String chamarPaginaListaQuestoes() {
        return getPaginaListaQuestao();
    }
    public String chamarPaginaListaQuestaoPorAreaENEM() {
        return getPaginaListaQuestaoENEM();
    }

    public String getPaginaCadastroUsuario() {
        return paginaCadastroUsuario;
    }

    public void setPaginaCadastroUsuario(String paginaCadastroUsuario) {
        this.paginaCadastroUsuario = paginaCadastroUsuario;
    }
    

    public String getPaginaCadastroDisciplinas() {
        return paginaCadastroDisciplinas;
    }

    public void setPaginaCadastroDisciplinas(String paginaCadastroDisciplinas) {
        this.paginaCadastroDisciplinas = paginaCadastroDisciplinas;
    }

    public String getPaginaPrincipal() {
        return paginaPrincipal;
    }

    public void setPaginaPrincipal(String paginaPrincipal) {
        this.paginaPrincipal = paginaPrincipal;
    }

    public String getPaginaListaQuestaoFiltradas() {
        return paginaListaQuestaoFiltradas;
    }

    public void setPaginaListaQuestaoFiltradas(String paginaListaQuestaoFiltradas) {
        this.paginaListaQuestaoFiltradas = paginaListaQuestaoFiltradas;
    }

    public String getPaginaListaQuestaoENEM() {
        return paginaListaQuestaoENEM;
    }

    public void setPaginaListaQuestaoENEM(String paginaListaQuestaoENEM) {
        this.paginaListaQuestaoENEM = paginaListaQuestaoENEM;
    }

}
