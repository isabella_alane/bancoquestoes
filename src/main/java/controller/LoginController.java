/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.UsuarioDAO;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import modelo.TbUsuario;

@ManagedBean(name = "loginController")
@SessionScoped
public class LoginController {

    private TbUsuario usuarioLogin = new TbUsuario();
    private boolean usuarioLogado = false;
    private String mensagem = "Teste";

    //Para validar o login digitado na janela de login
    public void validarLogin() {
        this.usuarioLogado = false;
        if (this.usuarioLogin == null) {
            usuarioLogin = new TbUsuario();
        }
        UsuarioDAO u = new UsuarioDAO();
        if (!this.usuarioLogin.getNmLogin().equalsIgnoreCase("") && !this.usuarioLogin.getDsSenha().equalsIgnoreCase("")) {
            this.usuarioLogin = u.loginTbUsuario(this.usuarioLogin);
            this.usuarioLogado = (this.usuarioLogin!=null );
            if (this.usuarioLogado) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Login realizado com sucesso."));
                this.setMensagem("Login realizado com sucesso.");
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro", "Login incorreto. Tente novamente."));
                this.setMensagem("Login incorreto. Tente novamente");
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Erro", "Login ou senha em branco. Favor preenchê-los."));
            this.setMensagem("Login ou senha em branco. Favor preenchê-los.");
        }

    }

    public String chamarPaginaPrincipal() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de cadastro");
            return a.getPaginaPrincipal();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }

    public String chamarPaginaLogin() {
        Paginas a = new Paginas();
        this.setMensagem("Você foi para a página de LOGIN");
        return a.getPaginaLogin();
    }
    
    public String fazerLogoff() {
        Paginas a = new Paginas();
        this.usuarioLogin.clearUsuario();
        this.usuarioLogado = false;
        this.setMensagem("Você foi para a página de LOGIN");
        return a.getPaginaLogin();
    }
    
    //Chamada da página de cadastrar resposta com um parâmetro da questão anterior
    public String chamarPaginaCadastroResposta(int idQuestaoAtual) {
        QuestaoController q = QuestaoController.getInstance();
        q.getQuestao().setIdQuestao(idQuestaoAtual);
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de cadastro");
            return a.getPaginaCadastroResposta();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }
    
    public String chamarPaginaCadastroRespostaLista() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de cadastro de resposta");
            return a.getPaginaCadastrarRespostasLista();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }

    public TbUsuario getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(TbUsuario usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(boolean usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public String chamarPaginaCadastroDisciplinas() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de cadastro");
            return a.getPaginaCadastroDisciplinas();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }
     public String chamarPaginaCadastroResposta() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de cadastro");
            return a.getPaginaCadastroResposta();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }
    public String chamarPaginaCadastroUsuario() {
        Paginas a = new Paginas();
        this.setMensagem("Você foi para a página de cadastro de um novo usuário!");
        return a.getPaginaCadastroUsuario();
    }

    public String chamarPaginaCadastroQuestoes() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de cadastro");
            return a.getPaginaCadastroQuestoes();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }
     public String chamarPaginaListaQuestao() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de listagem");
            return a.chamarPaginaListaQuestoes();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }
     public String chamarPaginaListaQuestaoPorArea() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de listagem");
            return a.chamarPaginaListaQuestaoPorAreaENEM();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }
     public String chamarPafinaListaRespostaFiltrada(){
         Paginas a = new Paginas();
         if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de listagem");
            return a.chamarPaginaListaQuestaoPorAreaENEM();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
     }
     
    public String chamarPaginaListaDisciplina() {
        Paginas a = new Paginas();
        if (this.usuarioLogado) {
            this.setMensagem("Login efetuado com SUCESSO! Você foi para a página de listagem");
            return a.getPaginaListaDisciplina();
        } else {
            this.setMensagem("Login incorreto. Tente novamente.");
            return a.getPaginaLogin();
        }
    }
   public static LoginController getInstance() {
        LoginController atual = (LoginController) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get("loginController");
        return atual;
    }
}
