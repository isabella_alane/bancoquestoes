/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_perfil")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPerfil.findAll", query = "SELECT t FROM TbPerfil t")
    , @NamedQuery(name = "TbPerfil.findByIdPerfil", query = "SELECT t FROM TbPerfil t WHERE t.idPerfil = :idPerfil")
    , @NamedQuery(name = "TbPerfil.findByNmPerfil", query = "SELECT t FROM TbPerfil t WHERE t.nmPerfil = :nmPerfil")
    , @NamedQuery(name = "TbPerfil.findByDsAcesso", query = "SELECT t FROM TbPerfil t WHERE t.dsAcesso = :dsAcesso")})
public class TbPerfil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_perfil")
    private Integer idPerfil;
    @Size(max = 100)
    @Column(name = "nm_perfil")
    private String nmPerfil;
    @Size(max = 100)
    @Column(name = "ds_acesso")
    private String dsAcesso;

    public TbPerfil() {
    }

    public TbPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public Integer getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(Integer idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getNmPerfil() {
        return nmPerfil;
    }

    public void setNmPerfil(String nmPerfil) {
        this.nmPerfil = nmPerfil;
    }

    public String getDsAcesso() {
        return dsAcesso;
    }

    public void setDsAcesso(String dsAcesso) {
        this.dsAcesso = dsAcesso;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPerfil != null ? idPerfil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPerfil)) {
            return false;
        }
        TbPerfil other = (TbPerfil) object;
        if ((this.idPerfil == null && other.idPerfil != null) || (this.idPerfil != null && !this.idPerfil.equals(other.idPerfil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbPerfil[ idPerfil=" + idPerfil + " ]";
    }
    
}
