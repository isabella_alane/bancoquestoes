/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_imagem_thumbnail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbImagemThumbnail.findAll", query = "SELECT t FROM TbImagemThumbnail t")
    , @NamedQuery(name = "TbImagemThumbnail.findByIdThumbnail", query = "SELECT t FROM TbImagemThumbnail t WHERE t.idThumbnail = :idThumbnail")})
public class TbImagemThumbnail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_thumbnail")
    private Integer idThumbnail;
    @Lob
    @Column(name = "im_imagem")
    private byte[] imImagem;
    @JoinColumn(name = "id_imagem", referencedColumnName = "id_imagem")
    @ManyToOne
    private TbImagem idImagem;

    public TbImagemThumbnail() {
    }

    public TbImagemThumbnail(Integer idThumbnail) {
        this.idThumbnail = idThumbnail;
    }

    public Integer getIdThumbnail() {
        return idThumbnail;
    }

    public void setIdThumbnail(Integer idThumbnail) {
        this.idThumbnail = idThumbnail;
    }

    public byte[] getImImagem() {
        return imImagem;
    }

    public void setImImagem(byte[] imImagem) {
        this.imImagem = imImagem;
    }

    public TbImagem getIdImagem() {
        return idImagem;
    }

    public void setIdImagem(TbImagem idImagem) {
        this.idImagem = idImagem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idThumbnail != null ? idThumbnail.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbImagemThumbnail)) {
            return false;
        }
        TbImagemThumbnail other = (TbImagemThumbnail) object;
        if ((this.idThumbnail == null && other.idThumbnail != null) || (this.idThumbnail != null && !this.idThumbnail.equals(other.idThumbnail))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbImagemThumbnail[ idThumbnail=" + idThumbnail + " ]";
    }
    
}
