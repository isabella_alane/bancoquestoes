/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_imagem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbImagem.findAll", query = "SELECT t FROM TbImagem t")
    , @NamedQuery(name = "TbImagem.findByIdImagem", query = "SELECT t FROM TbImagem t WHERE t.idImagem = :idImagem")})
public class TbImagem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_imagem")
    private Integer idImagem;
    @Lob
    @Column(name = "im_imagem")
    private byte[] imImagem;
    @OneToMany(mappedBy = "idImagem")
    private List<TbImagemThumbnail> tbImagemThumbnailList;

    public TbImagem() {
    }

    public TbImagem(Integer idImagem) {
        this.idImagem = idImagem;
    }

    public Integer getIdImagem() {
        return idImagem;
    }

    public void setIdImagem(Integer idImagem) {
        this.idImagem = idImagem;
    }

    public byte[] getImImagem() {
        return imImagem;
    }

    public void setImImagem(byte[] imImagem) {
        this.imImagem = imImagem;
    }

    @XmlTransient
    public List<TbImagemThumbnail> getTbImagemThumbnailList() {
        return tbImagemThumbnailList;
    }

    public void setTbImagemThumbnailList(List<TbImagemThumbnail> tbImagemThumbnailList) {
        this.tbImagemThumbnailList = tbImagemThumbnailList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idImagem != null ? idImagem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbImagem)) {
            return false;
        }
        TbImagem other = (TbImagem) object;
        if ((this.idImagem == null && other.idImagem != null) || (this.idImagem != null && !this.idImagem.equals(other.idImagem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbImagem[ idImagem=" + idImagem + " ]";
    }
    
}
