/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbUsuario.findAll", query = "SELECT t FROM TbUsuario t")
    , @NamedQuery(name = "TbUsuario.findByIdUsuario", query = "SELECT t FROM TbUsuario t WHERE t.idUsuario = :idUsuario")
    , @NamedQuery(name = "TbUsuario.findByNmUsuario", query = "SELECT t FROM TbUsuario t WHERE t.nmUsuario = :nmUsuario")
    , @NamedQuery(name = "TbUsuario.findByNmLogin", query = "SELECT t FROM TbUsuario t WHERE t.nmLogin = :nmLogin")
    , @NamedQuery(name = "TbUsuario.findByDsSenha", query = "SELECT t FROM TbUsuario t WHERE t.dsSenha = :dsSenha")})
public class TbUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_usuario")
    private Integer idUsuario;
    @Size(max = 200)
    @Column(name = "nm_usuario")
    private String nmUsuario;
    @Size(max = 50)
    @Column(name = "nm_login")
    private String nmLogin;
    @Size(max = 40)
    @Column(name = "ds_senha")
    private String dsSenha;
    @JoinColumn(name = "id_perfil", referencedColumnName = "id_perfil")
    @ManyToOne
    private TbPerfil idPerfil;
    @OneToMany(mappedBy = "idUsuario")
    private List<TbQuestao> tbQuestaoList;

    public TbUsuario() {
    }

    public TbUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNmUsuario() {
        return nmUsuario;
    }

    public void setNmUsuario(String nmUsuario) {
        this.nmUsuario = nmUsuario;
    }

    public String getNmLogin() {
        return nmLogin;
    }

    public void setNmLogin(String nmLogin) {
        this.nmLogin = nmLogin;
    }

    public String getDsSenha() {
        return dsSenha;
    }

    public void setDsSenha(String dsSenha) {
        this.dsSenha = dsSenha;
    }

    public TbPerfil getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(TbPerfil idPerfil) {
        this.idPerfil = idPerfil;
    }

    @XmlTransient
    public List<TbQuestao> getTbQuestaoList() {
        return tbQuestaoList;
    }

    public void setTbQuestaoList(List<TbQuestao> tbQuestaoList) {
        this.tbQuestaoList = tbQuestaoList;
    }
    
    public void clearUsuario(){
        this.setDsSenha("");
        this.setIdPerfil(null);
        this.setIdUsuario(null);
        this.setNmLogin("");
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbUsuario)) {
            return false;
        }
        TbUsuario other = (TbUsuario) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbUsuario[ idUsuario=" + idUsuario + " ]";
    }

}
