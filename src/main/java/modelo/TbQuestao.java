/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_questao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbQuestao.findAll", query = "SELECT t FROM TbQuestao t")
    , @NamedQuery(name = "TbQuestao.findByIdQuestao", query = "SELECT t FROM TbQuestao t WHERE t.idQuestao = :idQuestao")
    , @NamedQuery(name = "TbQuestao.findByNmQuestao", query = "SELECT t FROM TbQuestao t WHERE t.nmQuestao = :nmQuestao")
    , @NamedQuery(name = "TbQuestao.findByDsTextoQuestao", query = "SELECT t FROM TbQuestao t WHERE t.dsTextoQuestao = :dsTextoQuestao")})
public class TbQuestao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_questao")
    private Integer idQuestao;
    @Size(max = 150)
    @Column(name = "nm_questao")
    private String nmQuestao;
    @Size(max = 2147483647)
    @Column(name = "ds_texto_questao")
    private String dsTextoQuestao;
    
    @Column(name = "id_resposta")
    private Integer idResposta;

    @Column(name = "id_disciplina")
    private Integer idDisciplina;

    @Column(name = "id_area_enem")
    private Integer idAreaEnem;

    @Column(name = "id_area_conhecimento")
    private Integer idAreaConhecimento;

    @Column(name = "id_nivel")
    private Integer idNivel;

    @Column(name = "id_usuario")
    private Integer idUsuario;
    


    public TbQuestao() {
    }

    public TbQuestao(Integer idQuestao) {
        this.idQuestao = idQuestao;
    }
    
    public Integer getIdQuestao() {
        return idQuestao;
    }

    public void setIdQuestao(Integer idQuestao) {
        this.idQuestao = idQuestao;
    }

    public String getNmQuestao() {
        return nmQuestao;
    }

    public void setNmQuestao(String nmQuestao) {
        this.nmQuestao = nmQuestao;
    }

    public String getDsTextoQuestao() {
        return dsTextoQuestao;
    }

    public void setDsTextoQuestao(String dsTextoQuestao) {
        this.dsTextoQuestao = dsTextoQuestao;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idQuestao != null ? idQuestao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbQuestao)) {
            return false;
        }
        TbQuestao other = (TbQuestao) object;
        if ((this.idQuestao == null && other.idQuestao != null) || (this.idQuestao != null && !this.idQuestao.equals(other.idQuestao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbQuestao[ idQuestao=" + idQuestao + " ]";
    }

    public Integer getIdResposta() {
        return idResposta;
    }

    public void setIdResposta(Integer idResposta) {
        this.idResposta = idResposta;
    }

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(Integer idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public Integer getIdAreaEnem() {
        return idAreaEnem;
    }

    public Integer getIdAreaConhecimento() {
        return idAreaConhecimento;
    }

    public void setIdAreaEnem(Integer idAreaEnem) {
        this.idAreaEnem = idAreaEnem;
    }

    public void setIdAreaConhecimento(Integer idAreaConhecimento) {
        this.idAreaConhecimento = idAreaConhecimento;
    }

    
    
}
