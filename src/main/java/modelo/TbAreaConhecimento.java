/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_area_conhecimento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbAreaConhecimento.findAll", query = "SELECT t FROM TbAreaConhecimento t")
    , @NamedQuery(name = "TbAreaConhecimento.findByIdAreaConhecimento", query = "SELECT t FROM TbAreaConhecimento t WHERE t.idAreaConhecimento = :idAreaConhecimento")
    , @NamedQuery(name = "TbAreaConhecimento.findByNmAreaConhecimento", query = "SELECT t FROM TbAreaConhecimento t WHERE t.nmAreaConhecimento = :nmAreaConhecimento")})
public class TbAreaConhecimento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_area_conhecimento")
    private Integer idAreaConhecimento;
    @Size(max = 150)
    @Column(name = "nm_area_conhecimento")
    private String nmAreaConhecimento;
    @OneToMany(mappedBy = "idAreaConhecimento")
    private List<TbDisciplina> tbDisciplinaList;

    public TbAreaConhecimento() {
    }

    public TbAreaConhecimento(Integer idAreaConhecimento) {
        this.idAreaConhecimento = idAreaConhecimento;
    }

    public Integer getIdAreaConhecimento() {
        return idAreaConhecimento;
    }

    public void setIdAreaConhecimento(Integer idAreaConhecimento) {
        this.idAreaConhecimento = idAreaConhecimento;
    }

    public String getNmAreaConhecimento() {
        return nmAreaConhecimento;
    }

    public void setNmAreaConhecimento(String nmAreaConhecimento) {
        this.nmAreaConhecimento = nmAreaConhecimento;
    }

    @XmlTransient
    public List<TbDisciplina> getTbDisciplinaList() {
        return tbDisciplinaList;
    }

    public void setTbDisciplinaList(List<TbDisciplina> tbDisciplinaList) {
        this.tbDisciplinaList = tbDisciplinaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAreaConhecimento != null ? idAreaConhecimento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbAreaConhecimento)) {
            return false;
        }
        TbAreaConhecimento other = (TbAreaConhecimento) object;
        if ((this.idAreaConhecimento == null && other.idAreaConhecimento != null) || (this.idAreaConhecimento != null && !this.idAreaConhecimento.equals(other.idAreaConhecimento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbAreaConhecimento[ idAreaConhecimento=" + idAreaConhecimento + " ]";
    }
    
}
