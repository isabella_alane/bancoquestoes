/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_resposta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbResposta.findAll", query = "SELECT t FROM TbResposta t")
    , @NamedQuery(name = "TbResposta.findByIdResposta", query = "SELECT t FROM TbResposta t WHERE t.idResposta = :idResposta")
    , @NamedQuery(name = "TbResposta.findByNmResposta", query = "SELECT t FROM TbResposta t WHERE t.nmResposta = :nmResposta")
    , @NamedQuery(name = "TbResposta.findByCdAlternativa", query = "SELECT t FROM TbResposta t WHERE t.cdAlternativa = :cdAlternativa")
    , @NamedQuery(name = "TbResposta.findByFlCorreta", query = "SELECT t FROM TbResposta t WHERE t.flCorreta = :flCorreta")})
public class TbResposta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_resposta")
    private Integer idResposta;
    @Size(max = 2147483647)
    @Column(name = "nm_resposta")
    private String nmResposta;
    @Size(max = 5)
    @Column(name = "cd_alternativa")
    private String cdAlternativa;
    @Column(name = "fl_correta")
    private Boolean flCorreta;


    @Column(name = "id_questao")
    private Integer idQuestao;
    
    public TbResposta() {
    }

    public TbResposta(Integer idResposta) {
        this.idResposta = idResposta;
    }

    public Integer getIdResposta() {
        return idResposta;
    }

    public void setIdResposta(Integer idResposta) {
        this.idResposta = idResposta;
    }

    public String getNmResposta() {
        return nmResposta;
    }

    public void setNmResposta(String nmResposta) {
        this.nmResposta = nmResposta;
    }

    public String getCdAlternativa() {
        return cdAlternativa;
    }

    public void setCdAlternativa(String cdAlternativa) {
        this.cdAlternativa = cdAlternativa;
    }

    public Boolean getFlCorreta() {
        return flCorreta;
    }

    public void setFlCorreta(Boolean flCorreta) {
        this.flCorreta = flCorreta;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idResposta != null ? idResposta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbResposta)) {
            return false;
        }
        TbResposta other = (TbResposta) object;
        if ((this.idResposta == null && other.idResposta != null) || (this.idResposta != null && !this.idResposta.equals(other.idResposta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbResposta[ idResposta=" + idResposta + " ]";
    }

    public Integer getIdQuestao() {
        return idQuestao;
    }

    public void setIdQuestao(Integer idQuestao) {
        this.idQuestao = idQuestao;
    }
    
}
