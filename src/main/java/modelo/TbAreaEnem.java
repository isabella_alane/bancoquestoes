/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_area_enem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbAreaEnem.findAll", query = "SELECT t FROM TbAreaEnem t")
    , @NamedQuery(name = "TbAreaEnem.findByIdAreaEnem", query = "SELECT t FROM TbAreaEnem t WHERE t.idAreaEnem = :idAreaEnem")
    , @NamedQuery(name = "TbAreaEnem.findByNmAreaEnem", query = "SELECT t FROM TbAreaEnem t WHERE t.nmAreaEnem = :nmAreaEnem")
    , @NamedQuery(name = "TbAreaEnem.findByQtQuestoesEnem", query = "SELECT t FROM TbAreaEnem t WHERE t.qtQuestoesEnem = :qtQuestoesEnem")})
public class TbAreaEnem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_area_enem")
    private Integer idAreaEnem;
    @Size(max = 150)
    @Column(name = "nm_area_enem")
    private String nmAreaEnem;
    @Column(name = "qt_questoes_enem")
    private Short qtQuestoesEnem;
    @OneToMany(mappedBy = "idAreaEnem")
    private List<TbDisciplina> tbDisciplinaList;

    public TbAreaEnem() {
    }

    public TbAreaEnem(Integer idAreaEnem) {
        this.idAreaEnem = idAreaEnem;
    }

    public Integer getIdAreaEnem() {
        return idAreaEnem;
    }

    public void setIdAreaEnem(Integer idAreaEnem) {
        this.idAreaEnem = idAreaEnem;
    }

    public String getNmAreaEnem() {
        return nmAreaEnem;
    }

    public void setNmAreaEnem(String nmAreaEnem) {
        this.nmAreaEnem = nmAreaEnem;
    }

    public Short getQtQuestoesEnem() {
        return qtQuestoesEnem;
    }

    public void setQtQuestoesEnem(Short qtQuestoesEnem) {
        this.qtQuestoesEnem = qtQuestoesEnem;
    }

    @XmlTransient
    public List<TbDisciplina> getTbDisciplinaList() {
        return tbDisciplinaList;
    }

    public void setTbDisciplinaList(List<TbDisciplina> tbDisciplinaList) {
        this.tbDisciplinaList = tbDisciplinaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAreaEnem != null ? idAreaEnem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbAreaEnem)) {
            return false;
        }
        TbAreaEnem other = (TbAreaEnem) object;
        if ((this.idAreaEnem == null && other.idAreaEnem != null) || (this.idAreaEnem != null && !this.idAreaEnem.equals(other.idAreaEnem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbAreaEnem[ idAreaEnem=" + idAreaEnem + " ]";
    }
    
}
