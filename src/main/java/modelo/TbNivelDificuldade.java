/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "tb_nivel_dificuldade")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbNivelDificuldade.findAll", query = "SELECT t FROM TbNivelDificuldade t")
    , @NamedQuery(name = "TbNivelDificuldade.findByIdNivel", query = "SELECT t FROM TbNivelDificuldade t WHERE t.idNivel = :idNivel")
    , @NamedQuery(name = "TbNivelDificuldade.findByNmNivel", query = "SELECT t FROM TbNivelDificuldade t WHERE t.nmNivel = :nmNivel")
    , @NamedQuery(name = "TbNivelDificuldade.findByDsNivel", query = "SELECT t FROM TbNivelDificuldade t WHERE t.dsNivel = :dsNivel")})
public class TbNivelDificuldade implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nivel")
    private Integer idNivel;
    @Size(max = 50)
    @Column(name = "nm_nivel")
    private String nmNivel;
    @Size(max = 100)
    @Column(name = "ds_nivel")
    private String dsNivel;


    public TbNivelDificuldade() {
    }

    public TbNivelDificuldade(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public Integer getIdNivel() {
        return idNivel;
    }

    public void setIdNivel(Integer idNivel) {
        this.idNivel = idNivel;
    }

    public String getNmNivel() {
        return nmNivel;
    }

    public void setNmNivel(String nmNivel) {
        this.nmNivel = nmNivel;
    }

    public String getDsNivel() {
        return dsNivel;
    }

    public void setDsNivel(String dsNivel) {
        this.dsNivel = dsNivel;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNivel != null ? idNivel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbNivelDificuldade)) {
            return false;
        }
        TbNivelDificuldade other = (TbNivelDificuldade) object;
        if ((this.idNivel == null && other.idNivel != null) || (this.idNivel != null && !this.idNivel.equals(other.idNivel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.TbNivelDificuldade[ idNivel=" + idNivel + " ]";
    }
    
}
